package com.rdamyon.knime.service;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by rene on 01.06.17.
 */
@Service
public class FileServiceImpl implements FileService {


    private String TEMP_PATH;

    @Autowired
    public FileServiceImpl(@Value("${temp.path}") String path){
        TEMP_PATH = path;
    }

    /**
     * Should service return a file handle if file doesn't exist?
     * @param path
     * @return
     */
    @Override
    public File getFile(String path) {
        String completePath = TEMP_PATH + path;
        File file = FileUtils.getFile(completePath);
        return file.exists() ? file : null;
    }

    /**
     * TODO Delete directories?
     */
    @Override
    public String deleteFile(String pathString) {

        String completePathString = TEMP_PATH + pathString;
        File fileToDelete = new File(completePathString);
        if(FileUtils.deleteQuietly(fileToDelete)){
            return pathString;
        }
        return null;
    }

    /**
     * TODO copyToFile creates blank file even if copy fails
     * @param pathString
     * @param inputStream
     * @return
     */
    @Override
    public String postFile(String pathString, InputStream inputStream) {

        String completePathString = TEMP_PATH + pathString;
        File fileToCreate = new File(completePathString);
        try{
            //FileUtils creates directories recursively
            FileUtils.copyToFile(inputStream, fileToCreate);
        }catch(IOException e){
            System.out.println(e.getMessage());
            return null;
        }
        return pathString;
    }
}
