package com.rdamyon.knime.service;

import java.io.File;
import java.io.InputStream;

/**
 * Created by rene on 01.06.17.
 */
public interface FileService {

    File getFile(String pathString);

    String deleteFile(String pathString);

    String postFile(String pathString, InputStream inputStream);
}
