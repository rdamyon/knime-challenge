package com.rdamyon.knime.web;

import com.rdamyon.knime.service.FileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * Created by rene on 01.06.17.
 */
@RestController
public class FileController {

    FileServiceImpl fileService;

    /**
     * TODO Implement reading metadata
     * @param path
     * @param metadataRequested
     * @return
     */
    @GetMapping(value = "/api/file")
    public ResponseEntity<Resource> getResource(@RequestParam String path,
                        @RequestParam(value="metadata", required = false) Boolean metadataRequested){

        File file = fileService.getFile(path);

        if(file != null) {
            Resource resource = new FileSystemResource(file);
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        }else {
            return ResponseEntity.notFound().build();
        }

    }

    @PostMapping(value="/api/file")
    public String postResource(@RequestParam String path,
                            @RequestParam("file") MultipartFile file) throws Exception{

        return fileService.postFile(path, file.getInputStream());

    }

    @DeleteMapping(value = "/api/file")
    public String deleteResource(@RequestParam String path){
        return fileService.deleteFile(path);
    }

    @Autowired
    public void setFileService(FileServiceImpl fileService){
        this.fileService = fileService;
    }
}
