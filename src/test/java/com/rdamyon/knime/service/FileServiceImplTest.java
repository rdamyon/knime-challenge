package com.rdamyon.knime.service;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import static org.mockito.Mockito.*;

/**
 * Created by rene on 01.06.17.
 */
public class FileServiceImplTest {

    FileService fileService;

    static final String TEMP_PATH = "temp/";
    static final String SAMPLE_FILE_PATH = "src/test/resources/";
    static final String SAMPLE_FILE_NAME = "SampleFile";

    @Before
    public void setup() throws Exception{
        FileUtils.deleteQuietly(new File(TEMP_PATH));
        fileService = new FileServiceImpl(TEMP_PATH);
        Files.createDirectory(Paths.get(TEMP_PATH));
    }

    @Test
    public void testGetResourcePositive() throws Exception{
        Files.copy(Paths.get(SAMPLE_FILE_PATH + SAMPLE_FILE_NAME), Paths.get(TEMP_PATH + SAMPLE_FILE_NAME));
        File testFile = fileService.getFile(SAMPLE_FILE_NAME);
        assertTrue(testFile.exists());
    }

    @Test
    public void testGetResourceNegative() throws Exception{
        Files.copy(Paths.get(SAMPLE_FILE_PATH + SAMPLE_FILE_NAME), Paths.get(TEMP_PATH + SAMPLE_FILE_NAME));
        File testFile = fileService.getFile(SAMPLE_FILE_NAME + "1");
        assertFalse(testFile.exists());
    }

    @Test
    public void testDeleteResourcePositive() throws Exception{
        Files.copy(Paths.get(SAMPLE_FILE_PATH + SAMPLE_FILE_NAME), Paths.get(TEMP_PATH + SAMPLE_FILE_NAME));
        String deletedResource = fileService.deleteFile(SAMPLE_FILE_NAME);
        assertEquals(SAMPLE_FILE_NAME, deletedResource);
        assertFalse(Files.exists(Paths.get(TEMP_PATH + SAMPLE_FILE_NAME)));
    }

    @Test
    public void testDeleteResourceNegative() throws Exception{
        Files.copy(Paths.get(SAMPLE_FILE_PATH + SAMPLE_FILE_NAME), Paths.get(TEMP_PATH + SAMPLE_FILE_NAME));
        String deletedResource = fileService.deleteFile(SAMPLE_FILE_NAME + "1");
        assertNull(deletedResource);
        assertTrue(Files.exists(Paths.get(TEMP_PATH + SAMPLE_FILE_NAME)));
    }

    @Test
    public void testPostResourcePositive() throws Exception{
        InputStream stream = Files.newInputStream(Paths.get(SAMPLE_FILE_PATH + SAMPLE_FILE_NAME));
        String putResource = fileService.postFile(SAMPLE_FILE_NAME, stream);
        assertEquals(SAMPLE_FILE_NAME, putResource);
        assertTrue(Files.exists(Paths.get(TEMP_PATH + SAMPLE_FILE_NAME)));
        assertEquals(8, Files.size(Paths.get(TEMP_PATH + SAMPLE_FILE_NAME)));
    }

    @Test
    public void testPostResourceNegative() throws Exception{
        InputStream stream = mock(InputStream.class);
        when(stream.read(any())).thenThrow(IOException.class);
        String putResource = fileService.postFile(SAMPLE_FILE_NAME, stream);
        assertNull(putResource);
        assertFalse(Files.exists(Paths.get(TEMP_PATH + SAMPLE_FILE_NAME)));
        //assertEquals(0, Files.size(Paths.get(TEMP_PATH + SAMPLE_FILE_NAME)));
    }

    @After
    public void tearDown(){
        FileUtils.deleteQuietly(new File(TEMP_PATH));
    }
}
